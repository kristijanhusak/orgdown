★ [[Overview.org][← Overview page]] ★

* How to Contribute to Orgdown

There are many possible ways to support the idea of Orgdown:

- *Promote Orgdown* in your personal Internet bubble
- *Add tools* + verification tables: [[faq/how-to-add-tools.org][learn how]]
- *Improvements of this Orgdown web page* of all sorts
- Add *more parsers* that are able to parse at least Orgdown1
- *Improve* existing *parsers* or parser fragments
- Support *Orgdown in your own tools*

And if you are thinking really bold:

- [[Formal-Syntax-Definition.org][Add formal specification]] for specific Orgdown levels starting with
  Orgdown1. Since there are some ongoing formal definitions of
  Org-mode, this should not be too much of work.

- Create [[https://en.wikipedia.org/wiki/Language_Server_Protocol][a Language Server Protocol (LSP)]] for Orgdown so that tools
  supporting LSP can offer syntax highlighting.

- Help defining a process of moving the curation process for Orgdown
  from [[https://karl-voit.at/][Karl]] to a group of people. This mitigates the [[https://en.wikipedia.org/wiki/Bus_factor][bus factor]] and
  makes sure that Orgdown is based on a wider point of view.

- Implement (something like) [[https://en.wikipedia.org/wiki/Gemini_(protocol)][Gemini]] but with linked Orgdown1 files
  instead of [[https://communitywiki.org/wiki/GemText][this reduced Markdown files]] - how *awesome* this would
  be! Static web page generators could also generate gemini pages
  based on Orgdown1 syntax. More and more content could be made
  available in a web that lacks annoying things like malware,
  advertisements, poor browsing performance or irritating page layouts
  while adding much value for people on devices with small screens,
  people who are visually impaired, and so forth.
